import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by User on 12.02.2016.
 */
public class SimpleTest extends TestBase{
    @Test
    public void test01(final ITestContext testContext){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.id("org.wordpress.android:id/imageView")).sendKeys("test@test.com");
        System.out.println("Test 1 - run from "+testContext.getName());
    }
    @Test
    public void test02(final ITestContext testContext){
        driver.findElement(By.id("org.wordpress.android:id/nux_password")).sendKeys("pass");
        System.out.println("Test 2 - run from "+testContext.getName());
    }
    @Test
    public void test03(final ITestContext testContext){
        driver.findElement(By.id("org.wordpress.android:id/nux_sign_in_button")).click();
        System.out.println("Test 3 - run from "+testContext.getName());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
