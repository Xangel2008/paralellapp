import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by comp on 16.02.2016.
 */
public class TestBase {
    public AppiumDriver<MobileElement> driver;
    static String workingDir = System.getProperty("user.dir");
    DesiredCapabilities caps = new DesiredCapabilities();
    @BeforeClass
    @Parameters("deviceName")
    public void setUp(String deviceName) throws MalformedURLException {
        caps.setCapability("deviceName", deviceName);
        //caps.setCapability("platformVersion", "4.3");
        caps.setCapability("app", workingDir +"\\wordpress.apk");
        //caps.setCapability("package", "org.wordpress.android");
        caps.setCapability("appActivity", "org.wordpress.android.ui.WPLaunchActivity");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4444/wd/hub"), caps);
    }
    @AfterClass
    public void down(){
        driver.closeApp();
    }
}
